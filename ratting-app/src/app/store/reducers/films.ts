 import { Action } from '@ngrx/store';
import {createReducer, on} from '@ngrx/store';
import * as states from '../film.actions';
import {FilmsService} from '../../services/films.service';
import{Card} from '../../models/element';
//import { Cards } from 'src/app/models/mock';

export interface State{
    name:{[name:string]:Card};
    rating: {[rating:number]:Card};
}

export const initialState: State = {
name:{},
rating:{}
};


export  const init = 0;
const _counterReducer = createReducer(
    init, 
    on(states.incre,state => state++),
    on(states.decre, state=> state--),
    on(states.random, state=> Math.floor(Math.random()*(10-0)*0)),
    on(states.valor, (state,{num})=>num)
);

export function counterReducer(state, action){
    return _counterReducer(state,action);
} 
