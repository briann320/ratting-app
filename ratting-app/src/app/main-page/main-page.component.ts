import { Component, OnInit } from '@angular/core';
//import {Store,select} from '@ngrx/store';
import { FilmsService } from '../services/films.service';
import { OrderObservable } from '../services/films.service';
import { Observable} from 'rxjs';
import * as action from '../store/film.actions';
import {Card} from '../models/element';


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit{
  cards = [];
  tempCards =[];
  //count$:Observable<number>;
  theFilm: Card[]=[];
 
  
  constructor(private filmService: FilmsService, private orderObservable: OrderObservable/*  private store:Store<{count:number}>, */ 
  ) { }
 
  ngOnInit() {
    this.cards = this.filmService.getAllFilms();
   //this.count$ = this.store.pipe(select('count'));
    //this.tempCards = this.cards;
    const orderObservable:any = this.orderObservable.OrderObse(this.tempCards);
    orderObservable.subscribe((cards)=>{
      this.addStar(cards);
      this.reduceStar(cards);
      this.randomValue();
      this.randomUnit();
     });
   
  }
  ngDoCheck(){
    this.orderItems(this.cards);
  }
    
 
  orderItems(cards){
        console.log("ordenando...");
    return this.sortCards(cards);
        //return this.tempCards.sort((a,b)=>{return b.rating -a.rating});
  }

 
  addStar(element) {
    console.log("upgrade")
   let num = 0;
    //element.rating = this.store.dispatch(action.incre({num}));
      if (element.rating !== 10) {
      element.rating += 1; 

      console.log(element)
      return element;
    }
  }

  reduceStar(element) {
    console.log("downgrade")
    if (element.rating !== 0) {
      //element = element + this.store.dispatch(action.decre());
      element.rating -= 1;
      return element;
    }
  } 

  randomValue() {
   let tempCards =this.cards;
   for (let i =  0; i <= tempCards.length; i++) {
    tempCards[i].rating=Math.floor(Math.random() * (10 - 0)) + 0;
         }
         this.cards = tempCards
       return this.cards;
       
  }

  randomUnit(){
    
  this.cards;
    let random =Math.floor(Math.random() * (10 - 0)) + 0;
    this.cards[random].rating = Math.floor(Math.random() * (10 - 0)) + 0;
    console.log("elemento cambiado",this.cards[random])
    
      return this.cards;
  }

  
  sortCards(cards){
   let temp =cards.sort(
     function(a:any,b:any){
       return b.rating-a.rating
     }
    
   );
    console.log("elementos",temp)
    return temp

  }


}
