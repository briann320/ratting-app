import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreModule } from '@ngrx/store';
import {counterReducer} from '../store/reducers/films';
import { HttpModule } from '@angular/http';

import { OrderObservable } from '../services/films.service';


@NgModule({
  declarations: [],
  imports: [
    HttpModule,
    CommonModule,
    StoreModule.forRoot({
      count:counterReducer
    }),
  
  ],
  providers:[OrderObservable
  
  ]
})
export class MainPageModule { }
